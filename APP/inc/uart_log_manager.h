

#include "stm32f4xx_hal.h"
#include <string.h>
#include <cstdio>

class usart_log_manager
{

private:
	const char* msgInitial = "USART MANAGER STARTED:";
	const char* msgError = "ERROR: Log container is empty";
	char* eventContenerLogs = nullptr;


	DMA_HandleTypeDef* HandleTypeDef;
	UART_HandleTypeDef* huart2;

public:
	usart_log_manager(UART_HandleTypeDef* huart2, DMA_HandleTypeDef* HandleTypeDef) noexcept;
	// usart_log_manager(){}
	void print_log(const char* eventLog) noexcept;
	void print_logs_container() noexcept;
	void add_log_to_contener(const char* eventLog) noexcept;
	const char* get_log_contener() noexcept;

};

