#ifndef __LCD__
#define __LCD__

#include <stdint.h>
#include "stm32f4xx_hal.h"
#include "font.h"

#define LCD_DC			GPIO_Pin_8
#define LCD_CE			GPIO_Pin_9
#define LCD_RST			GPIO_Pin_13

#define PCD8544_FUNCTION_SET		0x20
#define PCD8544_DISP_CONTROL		0x08
#define PCD8544_DISP_NORMAL			0x0c
#define PCD8544_SET_Y				0x40
#define PCD8544_SET_X				0x80
#define PCD8544_H_TC				0x04
#define PCD8544_H_BIAS				0x10
#define PCD8544_H_VOP				0x80

#define LCD_BUFFER_SIZE			(84 * 48 / 8)

class lcd_nokia_5110
{
private:
	SPI_HandleTypeDef* hspi1_;
	uint8_t lcd_buffer[LCD_BUFFER_SIZE];
	uint16_t CE_PIN_;
	uint16_t DC_PIN_;
	uint16_t RST_PIN_;



public:
	lcd_nokia_5110(SPI_HandleTypeDef* hspi1, uint16_t CE_PIN, uint16_t DC_PIN, uint16_t RST_PIN): 
				hspi1_(hspi1), CE_PIN_(CE_PIN), DC_PIN_(DC_PIN), RST_PIN_(RST_PIN){}
	void cmd(uint8_t cmd);
	void reset();
	void setup(void);
	void clear(void);
	void draw_bitmap(const uint8_t* data);
	void draw_text(int row, int col, const char* text);
	void copy(void);
};


#endif // __LCD__