#include "stm32f4xx_hal.h"
#include <stdint.h>



class buzzer
{
private:
	uint16_t BUZZER_PIN_;
	TIM_HandleTypeDef* timmer10_;

	void rtc_timer_delay(uint16_t ms);

public:
	buzzer(uint16_t BUZZER_PIN, TIM_HandleTypeDef* timmer): BUZZER_PIN_(BUZZER_PIN), timmer10_(timmer){}
	void short_beep();
	void positive_soud();
	void negative_sound();
};