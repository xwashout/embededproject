class keypad{

static constexpr uint8_t col_count = 3;
static constexpr uint8_t row_count = 4;

public:
  
  uint8_t column_selected;
  uint8_t rows_selected;
  uint8_t key_map_table[col_count * row_count] = { '1', '2', '3', 
                                                   '4', '5', '6',
                                                   '7', '8', '9',
                                                   '*', '0', '#' };

  uint16_t row_map[row_count] = {};
  uint16_t col_map[col_count] = {};

  keypad();  
  uint8_t read_key();
};