#include "buzzer.h"

void buzzer::short_beep()
{
	HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_SET);
	rtc_timer_delay(100);
	HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_RESET);
}

void buzzer::rtc_timer_delay(uint16_t ms)
{
	if(ms > 500)
		return;
	HAL_TIM_Base_Start(timmer10_);
	while(__HAL_TIM_GET_COUNTER(timmer10_) < ms){}
	HAL_TIM_Base_Stop(timmer10_);
	__HAL_TIM_SET_COUNTER(timmer10_, 0);	
}


void buzzer::positive_soud()
{
	for(int i = 0 ; i < 3; i++)
	{
		HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_SET);
		rtc_timer_delay(40);
		HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_RESET);
		rtc_timer_delay(15);
	}
}
void buzzer::negative_sound()
{
	for(int i = 0 ; i < 3; i++)
	{
		HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_SET);
		rtc_timer_delay(100);
		HAL_GPIO_WritePin(GPIOC, BUZZER_PIN_, GPIO_PIN_RESET);
		rtc_timer_delay(40);
	}
}