#include <string.h>
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal_gpio.h"
#include "keypad.h"

keypad::keypad(){

	row_map[0] = GPIO_PIN_1;
	row_map[1] = GPIO_PIN_13;
	row_map[2] = GPIO_PIN_14;
	row_map[3] = GPIO_PIN_15;

	col_map[0] = GPIO_PIN_5;
	col_map[1] = GPIO_PIN_6;
	col_map[2] = GPIO_PIN_8;

}

uint8_t keypad::read_key(){

	GPIO_PinState state;
	
	while(true){
		for(column_selected = 0; column_selected < 3; column_selected++){
			HAL_GPIO_WritePin(GPIOC, col_map[column_selected], GPIO_PIN_SET); 
			for(rows_selected = 0; rows_selected < 4; rows_selected++){
				state = HAL_GPIO_ReadPin(GPIOB, row_map[rows_selected]);
				if(state == GPIO_PIN_SET){
					goto key_found;
				}
			}
			HAL_GPIO_WritePin(GPIOC, col_map[column_selected], GPIO_PIN_RESET); 	
		}
	}

	key_found:;
	HAL_GPIO_WritePin(GPIOC, col_map[column_selected], GPIO_PIN_RESET); 	
	return key_map_table[col_count * rows_selected + column_selected];
}