#include  "uart_log_manager.h"

usart_log_manager::usart_log_manager(UART_HandleTypeDef* huart2, DMA_HandleTypeDef *HandleTypeDef) noexcept 
{
	this->huart2 = huart2;
	this->HandleTypeDef = HandleTypeDef;
  	huart2->Instance->CR3 |= USART_CR3_DMAT;
	HAL_DMA_Start_IT(HandleTypeDef, (uint32_t)msgInitial, (uint32_t)huart2->Instance->DR, strlen(msgInitial));
}


void usart_log_manager::print_log(const char* eventLog) noexcept
{
	HAL_DMA_Start_IT(HandleTypeDef, (uint32_t)eventLog, (uint32_t)huart2->Instance->DR, strlen(eventLog));
}

void usart_log_manager::print_logs_container() noexcept
{
	if (eventContenerLogs == nullptr)
		{
			HAL_DMA_Start_IT(HandleTypeDef, 
						    (uint32_t)&msgError, 
						    (uint32_t)huart2->Instance->DR, 
						    strlen(msgError));
		}
		else
		{
			HAL_DMA_Start_IT(HandleTypeDef, 
						    (uint32_t)&eventContenerLogs, 
						    (uint32_t)huart2->Instance->DR, 
						    strlen(eventContenerLogs));
		}

}

void usart_log_manager::add_log_to_contener(const char* eventLog) noexcept
{
	sprintf(eventContenerLogs, "%c \n", eventLog);
}

const char* usart_log_manager::get_log_contener() noexcept
{
	if (eventContenerLogs == nullptr)
	{
		return msgError; 
	}
	else
	{
		return eventContenerLogs;		
	}
}