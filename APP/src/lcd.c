#include <string.h>
#include "lcd.h"



void lcd_nokia_5110::cmd(uint8_t cmd)
{
 HAL_GPIO_WritePin(GPIOA, DC_PIN_, GPIO_PIN_RESET);
 HAL_GPIO_WritePin(GPIOA, CE_PIN_, GPIO_PIN_RESET);
 HAL_SPI_Transmit(&hspi1, &cmd, 1, 1000);
 HAL_GPIO_WritePin(GPIOA, DC_PIN_, GPIO_PIN_SET);
 HAL_GPIO_WritePin(GPIOA, CE_PIN_, GPIO_PIN_SET);
}

void lcd_nokia_5110::reset(){
	HAL_GPIO_WritePin(GPIOC, RST_PIN_ ,GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOC, RST_PIN_ ,GPIO_PIN_SET);
}
void lcd_nokia_5110::setup(void)
{
	reset();
	cmd(0x21);
	cmd(0x14);
	cmd(0x80 | 0x3f); //Ustawienie kontrastu
	cmd(0x20);
	cmd(0x0c);
}

void lcd_nokia_5110::clear(void)
{
	memset(lcd_buffer, 0, LCD_BUFFER_SIZE);
}

void lcd_nokia_5110::draw_bitmap(const uint8_t* data)
{
	memcpy(lcd_buffer, data, LCD_BUFFER_SIZE);
}

void lcd_nokia_5110::draw_text(int row, int col, const char* text)
{
	int i;
	uint8_t* pbuf = &lcd_buffer[row * 84 + col];

	while ((*text) && (pbuf < &lcd_buffer[LCD_BUFFER_SIZE - 6])) {
		int ch = *text++;
		const uint8_t* font = &font_ASCII[ch - ' '][0];
		for (i = 0; i < 5; i++) {
			*pbuf++ = *font++;
		}
		*pbuf++ = 0;
	}
}

void lcd_nokia_5110::copy(void)
{
	
	HAL_GPIO_WritePin(GPIOA, CE_PIN_, GPIO_PIN_RESET);
 	HAL_GPIO_WritePin(GPIOA, DC_PIN_, GPIO_PIN_SET);
	HAL_SPI_Transmit(&hspi1, lcd_buffer, LCD_BUFFER_SIZE, HAL_MAX_DELAY);
	HAL_GPIO_WritePin(GPIOA, DC_PIN_, GPIO_PIN_RESET);
	HAL_GPIO_WritePin(GPIOA, CE_PIN_, GPIO_PIN_SET);
}