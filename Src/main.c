/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "keypad.h"
#include "lcd.h"
#include <string.h>
#include <string>
#include <cstdio>
#include "buzzer.h"

#include "main_menu.h"

#include "uart_log_manager.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

SPI_HandleTypeDef hspi1;

TIM_HandleTypeDef htim10;

UART_HandleTypeDef huart2;
DMA_HandleTypeDef hdma_usart2_tx;

/* USER CODE BEGIN PV */

keypad keypad_reader;
lcd_nokia_5110 lcd_driver(&hspi1, LCD_CE_Pin, LCD_DC_Pin, LCD_RST_Pin);
buzzer buz(BUZZER_Pin, &htim10);

int alarm_armed = 0;
int alarm_can_be_armed = 1;
int choosen_mode = 0;
int clear_block = 0;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_SPI1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM10_Init(void);
static void MX_I2C1_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
const char * msg = "TAP_KEY";
void DMATransferComplete(DMA_HandleTypeDef *hdma)
{

  huart2.Instance->CR3 &= ~USART_CR3_DMAT;
}

void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
  if(alarm_armed)
  {
    HAL_GPIO_WritePin(GPIOB, LED_ALARM_INDICATOR_Pin, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOC, SIREN_PIN_Pin, GPIO_PIN_SET);
  }

  if(alarm_can_be_armed)
  {
    alarm_can_be_armed = 0;
    HAL_GPIO_WritePin(GPIOB, MOTION_DETECT_LED_Pin, GPIO_PIN_RESET);
  }
  else
  {
    alarm_can_be_armed = 1;
    HAL_GPIO_WritePin(GPIOB, MOTION_DETECT_LED_Pin, GPIO_PIN_SET);
  }  
}

void arm_alarm()
{
  HAL_GPIO_WritePin(GPIOC, ALARM_STATE_LED_Pin, GPIO_PIN_SET);
  alarm_armed = 1;
}

void disarm_alarm()
{
  HAL_GPIO_WritePin(GPIOC, SIREN_PIN_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOC, ALARM_STATE_LED_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(GPIOB, LED_ALARM_INDICATOR_Pin, GPIO_PIN_RESET);
  
  alarm_armed = 0;
}

void open_doors()
{
  disarm_alarm();
  HAL_GPIO_WritePin(DOOR_LOCK_GPIO_Port, DOOR_LOCK_Pin, GPIO_PIN_SET);
  buz.positive_soud();
  HAL_Delay(2000);
  HAL_GPIO_WritePin(DOOR_LOCK_GPIO_Port, DOOR_LOCK_Pin, GPIO_PIN_RESET);
}




/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_SPI1_Init();
  MX_USART2_UART_Init();
  MX_TIM10_Init();
  MX_I2C1_Init();
  /* USER CODE BEGIN 2 */
  uint8_t key = '\0';

  std::string password("");
  // usart_log_manager usart_driver(&huart2, &hdma_usart2_tx);;

  // HAL_I2C_Mem_Write(&hi2c1, 0xa0, 0x00, 1, (uint8_t*)correct_password, 5, 1000);
  // HAL_Delay(1000);
  char correct_password[5];

  HAL_I2C_Mem_Read(&hi2c1, 0xa0, 0x00, 1, (uint8_t*)correct_password, 5, 1000);
  HAL_Delay(1000);
  char init_message[] = "1 - open doors2 - arm/dis";
  char password_text[] = "Password:";
  char key_buf[10];
  

  lcd_driver.setup();
  lcd_driver.clear();
  lcd_driver.draw_text(0, 0, init_message);
  lcd_driver.copy();


  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  while (1)
  {


    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
    huart2.Instance->CR3 |= USART_CR3_DMAT;
    
    if(!choosen_mode)
    {
      while(key != '1' && key != '2')
      {
        key = keypad_reader.read_key();
        if(key == '1')
        {
          choosen_mode = 1;
        }
        else
        {
          choosen_mode = 2;
        }
        HAL_Delay(200);
      }
    }
    lcd_driver.clear();
    lcd_driver.draw_text(0,0,password_text);
    lcd_driver.draw_text(1, 0, &password[0]);
    lcd_driver.copy();
    key = keypad_reader.read_key();
    if(key != '*' && key != '#')
    {
      buz.short_beep();
    }
    if(key == '*')
    {
      password.pop_back();
      lcd_driver.clear();
      lcd_driver.draw_text(0,0,password_text);
      lcd_driver.draw_text(1, 0, &password[0]);
      lcd_driver.copy();   
    }
    else if(key == '#')
    {
      lcd_driver.clear();

      if(password == correct_password)
      {
        if(choosen_mode == 1)
        {
          lcd_driver.draw_text(0,0, "OPEN");
          lcd_driver.copy();
          open_doors();

          choosen_mode = 0;
        }
        else
        {
          if(alarm_can_be_armed)
          {
            if(alarm_armed)
            {
              disarm_alarm();
            }
            else
            {
              arm_alarm();
            }
            choosen_mode = 0;
            clear_block = 0;
          }
          else
          {
            clear_block = 1;
          }
        }
      }
      else
      {
        lcd_driver.draw_text(0,0, "Access Denied");
        lcd_driver.copy();
        buz.negative_sound();
        HAL_Delay(1000);
      }
      if(!clear_block)
      {
        password.clear();
        lcd_driver.clear();
        lcd_driver.draw_text(0,0, init_message);
        lcd_driver.copy();
      }
    }
    else if(password.length() < 4)
    {
      password.push_back((char)key);
      lcd_driver.draw_text(1, 0, &password[0]);
      lcd_driver.copy();
    }  

    HAL_DMA_Start_IT(&hdma_usart2_tx, (uint32_t)key_buf, (uint32_t)&huart2.Instance->DR, strlen(key_buf));
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief SPI1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI1_Init(void)
{

  /* USER CODE BEGIN SPI1_Init 0 */

  /* USER CODE END SPI1_Init 0 */

  /* USER CODE BEGIN SPI1_Init 1 */

  /* USER CODE END SPI1_Init 1 */
  /* SPI1 parameter configuration*/
  hspi1.Instance = SPI1;
  hspi1.Init.Mode = SPI_MODE_MASTER;
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_SOFT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi1.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi1.Init.CRCPolynomial = 10;
  if (HAL_SPI_Init(&hspi1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI1_Init 2 */

  /* USER CODE END SPI1_Init 2 */

}

/**
  * @brief TIM10 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM10_Init(void)
{

  /* USER CODE BEGIN TIM10_Init 0 */

  /* USER CODE END TIM10_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM10_Init 1 */

  /* USER CODE END TIM10_Init 1 */
  htim10.Instance = TIM10;
  htim10.Init.Prescaler = 40000;
  htim10.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim10.Init.Period = 500;
  htim10.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim10.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_OC_Init(&htim10) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_TIMING;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_OC_ConfigChannel(&htim10, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM10_Init 2 */

  /* USER CODE END TIM10_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 921600;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOC, BUZZER_Pin|KEY_PAD_3_Pin|KEY_PAD_1_Pin|SIREN_PIN_Pin 
                          |KEY_PAD_5_Pin|ALARM_STATE_LED_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, DOOR_LOCK_Pin|MOTION_DETECT_LED_Pin|LED_ALARM_INDICATOR_Pin, GPIO_PIN_RESET);

  HAL_GPIO_WritePin(GPIOB, MOTION_DETECT_LED_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, LCD_CE_Pin|LCD_DC_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : BUZZER_Pin KEY_PAD_3_Pin KEY_PAD_1_Pin SIREN_PIN_Pin 
                           KEY_PAD_5_Pin ALARM_STATE_LED_Pin */
  GPIO_InitStruct.Pin = BUZZER_Pin|KEY_PAD_3_Pin|KEY_PAD_1_Pin|SIREN_PIN_Pin 
                          |KEY_PAD_5_Pin|ALARM_STATE_LED_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pin : LCD_RST_Pin */
  GPIO_InitStruct.Pin = LCD_RST_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LCD_RST_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : MOTION_IT_Pin */
  GPIO_InitStruct.Pin = MOTION_IT_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING_FALLING;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(MOTION_IT_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : KEY_PAD_2_Pin KEY_PAD_7_Pin KEY_PAD_6_Pin KEY_PAD_4_Pin */
  GPIO_InitStruct.Pin = KEY_PAD_2_Pin|KEY_PAD_7_Pin|KEY_PAD_6_Pin|KEY_PAD_4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : DOOR_LOCK_Pin */
  GPIO_InitStruct.Pin = DOOR_LOCK_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(DOOR_LOCK_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : LCD_CE_Pin LCD_DC_Pin */
  GPIO_InitStruct.Pin = LCD_CE_Pin|LCD_DC_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_PULLDOWN;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MOTION_DETECT_LED_Pin LED_ALARM_INDICATOR_Pin */
  GPIO_InitStruct.Pin = MOTION_DETECT_LED_Pin|LED_ALARM_INDICATOR_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI3_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
